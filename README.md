# COPA - Container Orchestration and Provisioning Architecture

COPA is a wireline/wireless convergent network monitoring and container manager architecture. This software architecture monitors resources and network quality of container-based virtualization hosts and enables the managements of containerized services through an API and an web interface. The objective of COPA is emulates a Cloud and Fog computing scenario that utilizes container-based virtualization in a converged network testbed infrastructure.

## COPA Pool
The COPA container Pool is where you can run containers and the computer that will be monitored.

*This tutorial takes in count that you have already installed LXD and CRIU*

For ubuntu 16.04: https://stgraber.org/2016/03/11/lxd-2-0-blog-post-series-012/

For ubuntu 18.04: Just install the latest versions of the Ubuntu repository.

### Installation Tutorial
*This tutorial was tested with Ubuntu 18.04 and 16.04*

First you need to upgrade your system.

    sudo apt -y update
    sudo apt -y upgrade

Create COPA's home directory

    sudo mkdir /copa
    sudo chown -R copa:copa /copa

Download copa code in copa folder. Do not forget the final dot in the git clone command.

    cd /copa
    git clone https://gitlab.com/futebol/COPA.git .
Install *python3-pip* to install the system requirements.

    sudo apt -y install python3-pip
Install the necessary python3 packages.

    cd /copa/pool
    pip3 install -r requirements.txt

In this step, if you are using ubuntu 16.04, maybe you will need to put *LC_ALL=en_US.utf8* in front of your command. Therefore:

    LC_ALL=en_US.utf8 pip3 install -r requirements.txt

### How to execute Pool

    cd /copa/pool
    ./pool.py start


## LXD Extra Configuration

In this file, you will find how to do the necessary adjustments in your LXD so COPA can manage it.

*In Ubuntu 18.04, the versions tested were: LXD 3.0.3 and CRIU 3.6.

In Ubuntu 16.04, the version tested were: LXD 2.0.9 and CRIU 3.1.*

### Initiate your LXD

First we need to initiate the LXD and add your user to the LXD group.

In our case we should use $USER = copa, but you can use another one.

    sudo usermod -aG lxd $USER
    lxd init --auto
    sudo chown -R $USER:$USER ~/.config/lxc
### To make lxd available in the network

Then, you need to make it available in the network. Also, we will add a trust password *copa*. We really recommend you use the same so the automatic scripts provided can work properly.

    lxc config set core.https_address [::]:8443
    lxc config set core.trust_password copa
    lxc config set core.https_allowed_origin [::]:8443
    lxc config set core.https_allowed_methods "GET, POST, PUT, DELETE, OPTIONS"
    lxc config set core.https_allowed_headers "Origin, X-Requested-With, Content-Type, Accept"

**DONE!**

## COPA Server 
This is the central entity of COPA. It runs the Webserver for the graphical interface, the database, the orchestrator and the monitor controller.
### Installation Tutorial

*This tutorial was tested with Ubuntu 18.04 and 16.04*

First you need to upgrade your system.

    sudo apt -y update 
    sudo apt -y upgrade

  Create COPA's home directory
  

```
sudo mkdir /copa
sudo chown -R copa:copa /copa
```

Download copa code in copa folder. Do not forget the final dot in the git clone command.

    cd /copa
    git clone https://gitlab.com/futebol/COPA.git .

Install *sshpass* for execution automation and *python3-pip* to install the system requirements.

    sudo apt -y install python3-pip sshpass

Remove the pytho3 cryptography package to not conflict with the one downloaded by pip.

    sudo apt -y remove python3-cryptography

Install the necessary python3 packages.

    cd /copa/server
    pip3 install -r requirements.txt

In this step, if you are using ubuntu 16.04, maybe you will need to put *LC_ALL=en_US.utf8* in front of your command. Therefore:

    LC_ALL=en_US.utf8 pip3 install -r requirements.txt

Now, we need to prepare the django database so we can start our webserver later.

    cd /copa/server/webserver
    python3 manage.py makemigrations
    python3 manage.py migrate
    python3 manage.py loaddata initconfig

Add a user in the database. In our example, we created a user named *copa* with a password *copa*.
 

    echo "from django.contrib.auth.models import User; User.objects.create_superuser('copa', 'copa@example.com', 'copa')" | python3 manage.py shell

### How to execute Server

Change the config file to your settings

    cd /copa/server
    vim config.json

The file has the following structure.
`{
	"user": "copa",
	"password": "copa",
	"server_ip": "10.0.2.11",
	"pools": [
	{
		"label": "Server1",
		"ip": "10.0.2.9"
	},
	{
		"label": "Server2",
		"ip": "10.0.2.10"
	}
	]
}`
 In *user* and *password* you need to put the standard user and password of yours machines hosting the pools. In *server_ip* you must put the ip from the COPA Server. In the pools list you need to add objects with *label* and *ip* of the existing COPA Pools. 

 **It is important that the labels do not have spaces or special characters.**

After setting up the IPs and users of the COPA Servers and Pools, its time to run some commands in other to execute the configuration that you setup in the configuration file. Execute the following command in */copa/server*

    ./copa.py configure

  In order to make COPA enabled for acessing LXD API from the COPA Pools. We need to setup some certificates. Fortunatelly, we have a automated script that create the certificate and distribute it automatically to the COPA Pools through *ssh*. In the folder */copa/server/certs* you need to run:

    ./automated_cert.py
Then, you can comeback in the */copa/server* and run:

**It is important that the pools are already running before you start the server.**

    ./copa.py start
