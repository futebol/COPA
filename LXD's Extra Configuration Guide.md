
# LXD Extra Configuration

In this file, you will find how to do the necessary adjustments in your LXD so COPA can manage it.

*In Ubuntu 18.04, the versions tested were: LXD 3.0.3 and CRIU 3.6.
In Ubuntu 16.04, the version tested were: LXD 2.0.9 and CRIU 3.1.*

## Initiate your LXD

First we need to initiate the LXD and add your user to the LXD group.
In our case we should use $USER = copa, but you can use another one.

    sudo usermod -aG lxd $USER
    lxd init --auto
    sudo chown -R $USER:$USER ~/.config/lxc
## To make lxd available in the network

Then, you need to make it available in the network. Also, we will add a trust password *copa*. We really recommend you use the same so the automatic scripts provided can work properly.

    lxc config set core.https_address [::]:8443
    lxc config set core.trust_password copa
    lxc config set core.https_allowed_origin [::]:8443
    lxc config set core.https_allowed_methods "GET, POST, PUT, DELETE, OPTIONS"
    lxc config set core.https_allowed_headers "Origin, X-Requested-With, Content-Type, Accept"

  

**DONE!**
