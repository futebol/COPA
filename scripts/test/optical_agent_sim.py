import requests
from threading import Thread
import random
from pprint import pprint
import time
from simulator import COPASimulator

class OpticalSimulator(COPASimulator):
    def __init__(self):
        COPASimulator.__init__(self)

    def generateData(self):
        bits_in = random.randint(0, 100)
        bits_out = random.randint(0, 100)

        return {
            "locus": self.pool,
            "func_name": "func"+str(random.randint(1,2)), 
            "port_number": random.randint(1,3), 
            "in": bits_in, 
            "out": bits_out,
            "total": bits_out+bits_in, 
        }

if __name__ == '__main__':
    sim = OpticalSimulator()
    sim.setPath("/kpioptical")
    sim.start()

    try:
        while True:
            pass
    except:
        sim.stop_thread()
