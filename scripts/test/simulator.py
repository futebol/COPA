import requests
from threading import Thread
import random
from pprint import pprint
import time

class COPASimulator(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.exec_flag = True
        self.addr = "10.0.2.8"
        self.protocol = "http"
        self.port = 8000
        self.rooturl = "%s://%s:%d" % (self.protocol, self.addr, self.port)
        self.rootnetwork = "%s/REST/network" % (self.rooturl)
        self.interval = 1
        self.pool = self.getPool()
        self.path = "/"

        print("Pool chosen: "+self.pool)

    def generateData(self):
        raise NotImplementedError()
        
    def setPool(self, pool):
        self.pool = pool

        return True
    
    def setPath(self, path):
        if not path.endswith("/"):
            path = path + "/"
        
        self.path = path

        return True

    def getPool(self):
        url = "%s/REST/pools" % self.rooturl
        
        response = requests.get(url)
        data = response.json()
        pprint(data)

        if data:
            if data["pools"]:
                pools = data["pools"]
                if pools[0]:
                    return pools[0][0]

        return None

    def sendData(self, data):
        url = self.rootnetwork+self.path
        response = requests.post(url, data)

        if response.status_code == 200:
            pprint(data)
            pprint(response.json())
        else:
            print("Error: ", response.status_code)
            print("Error: ", response.text)
        
        return True

    def run(self):
        while self.exec_flag:
            try:
                data = self.generateData()

                self.sendData(data)

                time.sleep(self.interval)
            except:
                print("Something gone wrong at COPASimulator.")
                self.exec_flag = False
                raise

    def stop_thread(self):
        self.exec_flag = False