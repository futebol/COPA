#!/bin/bash
# Script to turn a raw Ubuntu 16.04 into COPA Server or Pool

# Check if running as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

COPA_HOME=/copa
LXD_PASSWORD=copa
MY_USER=copa

apt update 

# Installs necessary kernel
apt install -y linux-headers-4.8.0-54-generic linux-headers-4.8.0-54 linux-image-4.8.0-54-generic linux-image-extra-4.8.0-54-generic

# Necessary for bridge
apt install -y bridge-utils avahi-daemon

# Necessary for criu
apt install -y make
apt install -y build-essential libc6-dev-i386 gcc-multilib libprotobuf-dev libprotobuf-c0-dev protobuf-c-compiler protobuf-compiler python-protobuf pkg-config python-ipaddr libbsd-dev iproute2 libcap-dev libnl-3-dev libnet-dev asciidoc

# This is for nodejs
#apt install -y npm

# Configures bridge
bash -c 'cat << EOF > /etc/network/interfaces
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
#auto ens3
#iface ens3 inet dhcp

auto br0
iface br0 inet dhcp
    bridge_ports ens3
EOF'

sed -i -- 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf

# Removes default lxd version of Ubuntu 16.04
if [ "$(lxd --version)" != "2.0.9" ]; then
    apt purge lxc* lxd* -y
    find / -name "*lxd*" | grep -v "$COPA_HOME" | sudo xargs rm -rf \;
    find / -name "*lxc*" | grep -v "$COPA_HOME" | sudo xargs rm -rf \;
fi

# Removes default criu version of Ubuntu 16.04
if [ "$(criu --version 2> /dev/null | cut -d" " -f2)" != "3.1" ]; then
    apt purge criu* -y
fi

# Creates copa folder
if [ ! -d "$COPA_HOME" ]; then
    mkdir $COPA_HOME
    chown -R $MY_USER:$MY_USER $COPA_HOME
fi

# Downloads COPA Code
sudo -u $MY_USER git clone https://gitlab.com/futebol/COPA.git $COPA_HOME

# Turn on bridge br0 and flush ens3 to restart network without losing ssh connection
(ifup br0 && ip addr flush dev ens3)

sleep 10s

service networking restart

#########################
# START OF OLD SCRIPT 2 #
#########################

cd $COPA_HOME/

# Installs criu if version different from 3.1
if [ "$(criu --version 2> /dev/null | cut -d" " -f2)" != "3.1" ]; then
    cd $COPA_HOME/live-migration/dependencies/criu
    sudo -u $MY_USER tar -xf criu-3.1.tar.bz2
    cd criu-3.1
    sudo -u $MY_USER make clean
    sudo -u $MY_USER make
    make install
fi

# Install LXD 2.0.9
# Find the Debian packages in the copa-dependencies folder and install them using:
if [ "$(lxd --version)" != "2.0.9" ]; then
    cd $COPA_HOME/live-migration/dependencies/lxd
    #This will produce an error (missing dependencies).
    dpkg -i lxd_2.0.9-0ubuntu1-14.04.1_amd64.deb lxd-client_2.0.9-0ubuntu1-14.04.1_amd64.deb  

    # To fix the dependencies error
    apt install -f -y  

    # To Downgrade LXD to the correct version.cd ..
    dpkg -i lxd_2.0.9-0ubuntu1-14.04.1_amd64.deb lxd-client_2.0.9-0ubuntu1-14.04.1_amd64.deb

    # Run the following command and follow its instructions. When it ask if you want to LXD to be available over the network, answer yes and setup the standard access IP, PORT and PASSWORD.
    usermod -aG lxd $MY_USER
    lxd init --auto

    # To make lxd available in the network
    lxc config set core.https_address [::]:8443
    lxc config set core.trust_password $LXD_PASSWORD

    lxc config set core.https_allowed_origin [::]:8443
    lxc config set core.https_allowed_methods "GET, POST, PUT, DELETE, OPTIONS"
    lxc config set core.https_allowed_headers "Origin, X-Requested-With, Content-Type, Accept"

    #*DO NOT CONFIGURE IPv4 or IPv6 SUBNET*
    lxc profile device set default eth0 parent enp0s3

    sed -i 's/USE_LXD_BRIDGE="true"/USE_LXD_BRIDGE="false"/g' /etc/default/lxd-bridge #disable the bridge created by the lxd init
    sed -i 's/UPDATE_PROFILE="true"/UPDATE_PROFILE="false"/g' /etc/default/lxd-bridge

	sudo chown -R $MY_USER:$MY_USER /home/$MY_USER/.config/lxc
fi

#########################
# Start of OLD script 3 #
#########################

chown -R $MY_USER:$MY_USER $COPA_HOME

cd $COPA_HOME/

LC_ALL=en_US.utf8 pip3 install --default-timeout=100 psutil
apt-get install libi2util-dev

mkdir owamp
cd owamp
sudo -u $MY_USER wget http://software.internet2.edu/sources/owamp/owamp-3.4-10.tar.gz
sudo -u $MY_USER tar xzf owamp-3.4-10.tar.gz
cd owamp-3.4
sudo -u $MY_USER ./configure
sudo -u $MY_USER make 
make install

sudo owampd -c $COPA_HOME/owamp/owamp-3.4/conf -v -U $MY_USER -G $MY_USER

if grep -Fxq "python3 monitor.py" /etc/rc.local; then 
	sed -i "/exit 0/i \
cd $COPA_HOME/ \n\
owampd -c $COPA_HOME/owamp/owamp-3.4/conf -v -U $MY_USER -G $MY_USER\n\
cd $COPA_HOME/copa_monitor \n\
python3 monitor.py" /etc/rc.local
fi


