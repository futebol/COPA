#!/bin/bash
# Script to turn a raw Ubuntu 16.04 into COPA Server or Pool

# Check if running as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

COPA_HOME=/copa
LXD_PASSWORD=copa
MY_USER=copa
NODE_HOME=$COPA_HOME/containers_site/core/static/core/xterm/

apt update 

# Installs necessary kernel
apt install -y linux-headers-4.8.0-54-generic linux-headers-4.8.0-54 linux-image-4.8.0-54-generic linux-image-extra-4.8.0-54-generic

# Necessary for bridge
apt install -y bridge-utils avahi-daemon

# Necessary for criu
apt install -y make
apt install -y build-essential libc6-dev-i386 gcc-multilib libprotobuf-dev libprotobuf-c0-dev protobuf-c-compiler protobuf-compiler python-protobuf pkg-config python-ipaddr libbsd-dev iproute2 libcap-dev libnl-3-dev libnet-dev asciidoc

# This is for nodejs
apt install -y npm

exit 0

# Configures bridge
bash -c 'cat << EOF > /etc/network/interfaces
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
#auto ens3
#iface ens3 inet dhcp

auto br0
iface br0 inet dhcp
    bridge_ports ens3
EOF'

sed -i -- 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf

# Removes default lxd version of Ubuntu 16.04
if [ "$(lxd --version)" != "2.0.9" ]; then
    apt purge lxc* lxd* -y
    find / -name "*lxd*" | grep -v "$COPA_HOME" | sudo xargs rm -rf \;
    find / -name "*lxc*" | grep -v "$COPA_HOME" | sudo xargs rm -rf \;
fi

# Removes default criu version of Ubuntu 16.04
if [ "$(criu --version 2> /dev/null | cut -d" " -f2)" != "3.1" ]; then
    apt purge criu* -y
fi

# Creates copa folder
if [ ! -d $COPA_HOME ]; then
    mkdir $COPA_HOME
    chown -R $MY_USER:$MY_USER $COPA_HOME
fi

# Downloads COPA Code
sudo -u $MY_USER git clone https://gitlab.com/futebol/COPA.git $COPA_HOME

# Turn on bridge br0 and flush ens3 to restart network without losing ssh connection
(ifup br0 && ip addr flush dev ens3)

sleep 10s

service networking restart

#########################
# START OF OLD SCRIPT 2 #
#########################

cd $COPA_HOME/

# Installs criu if version different from 3.1
if [ "$(criu --version 2> /dev/null | cut -d" " -f2)" != "3.1" ]; then
    cd $COPA_HOME/live-migration/dependencies/criu
    sudo -u $MY_USER tar -xf criu-3.1.tar.bz2
    cd criu-3.1
    sudo -u $MY_USER make clean
    sudo -u $MY_USER make
    make install
fi

# Install LXD 2.0.9
# Find the Debian packages in the copa-dependencies folder and install them using:
if [ "$(lxd --version)" != "2.0.9" ]; then
    cd $COPA_HOME/live-migration/dependencies/lxd
    #This will produce an error (missing dependencies).
    dpkg -i lxd_2.0.9-0ubuntu1-14.04.1_amd64.deb lxd-client_2.0.9-0ubuntu1-14.04.1_amd64.deb  

    # To fix the dependencies error
    apt install -f -y  

    # To Downgrade LXD to the correct version.cd ..
    dpkg -i lxd_2.0.9-0ubuntu1-14.04.1_amd64.deb lxd-client_2.0.9-0ubuntu1-14.04.1_amd64.deb


    # Run the following command and follow its instructions. When it ask if you want to LXD to be available over the network, answer yes and setup the standard access IP, PORT and PASSWORD.
    usermod -aG lxd $MY_USER
    lxd init --auto

    # To make lxd available in the network
    lxc config set core.https_address [::]:8443
    lxc config set core.trust_password $LXD_PASSWORD

    lxc config set core.https_allowed_origin [::]:8443
    lxc config set core.https_allowed_methods "GET, POST, PUT, DELETE, OPTIONS"
    lxc config set core.https_allowed_headers "Origin, X-Requested-With, Content-Type, Accept"

    #*DO NOT CONFIGURE IPv4 or IPv6 SUBNET*
    lxc profile device set default eth0 parent br0

    sed -i 's/USE_LXD_BRIDGE="true"/USE_LXD_BRIDGE="false"/g' /etc/default/lxd-bridge #disable the bridge created by the lxd init
    sed -i 's/UPDATE_PROFILE="true"/UPDATE_PROFILE="false"/g' /etc/default/lxd-bridge

	sudo chown -R $MY_USER:$MY_USER /home/$MY_USER/.config/lxc
fi

#########################
# Start of OLD script 3 #
#########################

chown -R $MY_USER:$MY_USER $COPA_HOME

cd $COPA_HOME/webserver/

apt install python3-pip -y
LC_ALL=en_US.utf8 pip3 install --default-timeout=100 -r requirements.txt

bash -c "cat << EOF > $COPA_HOME/package.json
{
 \"name\": \"copa\",
 \"version\": \"1.0.0\",
 \"description\": \"\",
 \"main\": \"index.js\",
 \"scripts\": {
   \"test\": \"echo \"Error: no test specified\" && exit 1\"
 },
 \"author\": \"\",
 \"license\": \"ISC\"
}
EOF"

apt-get install -y nodejs-legacy
cd $NODE_HOME
npm install
#npm install express socket.io term.js pty

##################################
# NEW PART OF SCRIPT COPA SERVER #
##################################

COPA_IP=$(ifconfig br0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)
echo "MY IP IS $COPA_IP"

#cd $COPA_HOME/certs;
#rm *.crt; rm *.key;
#sed -i "s/\("subjectAltName" *= *IP:\).*/\1$COPA_IP/" openssl.cnf;
#sh makeCOPA-CA.sh > /dev/null 2>&1;
#sh makeCOPAcerts.sh > /dev/null 2>&1

cd $COPA_HOME/containers_site
sudo -u $MY_USER python3 manage.py makemigrations
sudo -u $MY_USER python3 manage.py migrate
sudo -u $MY_USER python3 manage.py loaddata initconfig

CONFIG_COPA_PATH=$COPA_HOME/containers_site/containers_site/COPA_general.py
NODEJS_COPA_PATH=$COPA_HOME/containers_site/core/static/core/xterm/dist/server_socket.js

# Changing COPA Path
VMCOPA_PATH_ESC=$(echo $COPA_HOME/ | sed -r 's/\//\\\//g')
sed -i "s/\("COPA_HOME" *= \).*/\1\"$VMCOPA_PATH_ESC\"/" $CONFIG_COPA_PATH
sed -i "s/\("COPA_HOST" *= \).*/\1\"$COPA_IP\"/" $CONFIG_COPA_PATH
sed -i "s/\("IMAGE_HOST" *= \).*/\1\"$COPA_IP\"/" $CONFIG_COPA_PATH
sed -i "s/\("COPA_HOME" *= \).*/\1\"$VMCOPA_PATH_ESC\"/" $NODEJS_COPA_PATH

cd $COPA_HOME/containers_site/
echo "from django.contrib.auth.models import User; User.objects.create_superuser('copa', 'copa@example.com', 'copa')" | python3 manage.py shell

