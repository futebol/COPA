#!/bin/bash
# Script to turn a raw Ubuntu 16.04 into COPA Server or Pool

# Check if running as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

COPA_HOME=/copa
LXD_PASSWORD=copa
MY_USER=copa
export LC_ALL=en_US.utf8

apt -y update && apt -y upgrade

apt -y install criu python3-pip libi2util-dev

apt -y update && apt -y upgrade

pip3 install --default-timeout=100 psutil

# Configures bridge
bash -c 'cat << EOF > /etc/network/interfaces
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
#auto ens3
#iface ens3 inet dhcp

auto br0
iface br0 inet dhcp
    bridge_ports ens0p3
EOF'

sed -i -- 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf

# Creates copa folder
if [ ! -d "$COPA_HOME" ]; then
    mkdir $COPA_HOME
    chown -R $MY_USER:$MY_USER $COPA_HOME

    # Downloads COPA Code
    sudo -u $MY_USER git clone https://gitlab.com/futebol/COPA.git $COPA_HOME
    chown -R $MY_USER:$MY_USER $COPA_HOME
fi

chown -R $MY_USER:$MY_USER $COPA_HOME

# Turn on bridge br0 and flush ens3 to restart network without losing ssh connection
(ifup br0 && ip addr flush dev ens3)

sleep 10s

service networking restart

cd $COPA_HOME/

git checkout copa-org-merge-oldversion

chown -R $MY_USER:$MY_USER $COPA_HOME

# Run the following command and follow its instructions. When it ask if you want to LXD to be available over the network, answer yes and setup the standard access IP, PORT and PASSWORD.
usermod -aG lxd $MY_USER
lxd init --auto

# To make lxd available in the network
lxc config set core.https_address [::]:8443
lxc config set core.trust_password $LXD_PASSWORD

lxc config set core.https_allowed_origin [::]:8443
lxc config set core.https_allowed_methods "GET, POST, PUT, DELETE, OPTIONS"
lxc config set core.https_allowed_headers "Origin, X-Requested-With, Content-Type, Accept"

#*DO NOT CONFIGURE IPv4 or IPv6 SUBNET*
lxc profile device set default eth0 parent br0

sed -i 's/USE_LXD_BRIDGE="true"/USE_LXD_BRIDGE="false"/g' /etc/default/lxd-bridge #disable the bridge created by the lxd init
sed -i 's/UPDATE_PROFILE="true"/UPDATE_PROFILE="false"/g' /etc/default/lxd-bridge

chown -R $MY_USER:$MY_USER /home/$MY_USER/.config/lxc

# chown -R $MY_USER:$MY_USER $COPA_HOME


