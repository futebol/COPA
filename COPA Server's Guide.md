# COPA Server 
This is the central entity of COPA. It runs the Webserver for the graphical interface, the database, the orchestrator and the monitor controller.
## Installation Tutorial

*This tutorial was tested with Ubuntu 18.04 and 16.04*
First you need to upgrade your system.

    sudo apt -y update 
    sudo apt -y upgrade

  Create COPA's home directory
  

```
sudo mkdir /copa
sudo chown -R copa:copa /copa
```

Download copa code in copa folder. Do not forget the final dot in the git clone command.

    cd /copa
    git clone https://gitlab.com/futebol/COPA.git .

Install *sshpass* for execution automation and *python3-pip* to install the system requirements.

    sudo apt -y install python3-pip sshpass

Remove the pytho3 cryptography package to not conflict with the one downloaded by pip.

    sudo apt -y remove python3-cryptography

Install the necessary python3 packages.

    cd /copa/server
    pip3 install -r requirements.txt

In this step, if you are using ubuntu 16.04, maybe you will need to put *LC_ALL=en_US.utf8* in front of your command. Therefore:

    LC_ALL=en_US.utf8 pip3 install -r requirements.txt

Now, we need to prepare the django database so we can start our webserver later.

    cd /copa/server/webserver
    python3 manage.py makemigrations
    python3 manage.py migrate
    python3 manage.py loaddata initconfig

Add a user in the database. In our example, we created a user named *copa* with a password *copa*.
 

    echo "from django.contrib.auth.models import User; User.objects.create_superuser('copa', 'copa@example.com', 'copa')" | python3 manage.py shell

## How to execute Server

Change the config file to your settings

    cd /copa/server
    vim config.json

The file has the following structure.
`{
	"user": "copa",
	"password": "copa",
	"server_ip": "10.0.2.11",
	"pools": [
	{
		"label": "Server1",
		"ip": "10.0.2.9"
	},
	{
		"label": "Server2",
		"ip": "10.0.2.10"
	}
	]
}`
 In *user* and *password* you need to put the standard user and password of yours machines hosting the pools. In *server_ip* you must put the ip from the COPA Server. In the pools list you need to add objects with *label* and *ip* of the existing COPA Pools. 
 **It is important that the labels do not have spaces or special characters.**

After setting up the IPs and users of the COPA Servers and Pools, its time to run some commands in other to execute the configuration that you setup in the configuration file. Execute the following command in */copa/server*

    ./copa.py configure

  In order to make COPA enabled for acessing LXD API from the COPA Pools. We need to setup some certificates. Fortunatelly, we have a automated script that create the certificate and distribute it automatically to the COPA Pools through *ssh*. In the folder */copa/server/certs* you need to run:

    ./automated_cert.py
Then, you can comeback in the */copa/server* and run:
**It is important that the pools are already running before you start the server.**

    ./copa.py start



