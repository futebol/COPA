
# COPA Pool
The COPA container Pool is where you can run containers and the computer that will be monitored.

*This tutorial takes in count that you have already installed LXD and CRIU*
For ubuntu 16.04: https://stgraber.org/2016/03/11/lxd-2-0-blog-post-series-012/
For ubuntu 18.04: Just install the latest versions of the Ubuntu repository.
## Installation Tutorial
*This tutorial was tested with Ubuntu 18.04 and 16.04*
First you need to upgrade your system.

    sudo apt -y update
    sudo apt -y upgrade

Create COPA's home directory

    sudo mkdir /copa
    sudo chown -R copa:copa /copa

Download copa code in copa folder. Do not forget the final dot in the git clone command.

    cd /copa
    git clone https://gitlab.com/futebol/COPA.git .
Install *python3-pip* to install the system requirements.

    sudo apt -y install python3-pip
Install the necessary python3 packages.

    cd /copa/pool
    pip3 install -r requirements.txt

In this step, if you are using ubuntu 16.04, maybe you will need to put *LC_ALL=en_US.utf8* in front of your command. Therefore:

    LC_ALL=en_US.utf8 pip3 install -r requirements.txt

## How to execute Pool

    cd /copa/pool
    ./pool.py start

