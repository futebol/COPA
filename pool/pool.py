#! /usr/bin/python3
import os, sys, subprocess, signal
import datetime
import json
import re

dir_path = os.path.dirname(os.path.realpath(__file__))

logfilename = str(datetime.datetime.now().replace(microsecond=0))

class Pool():
    def __init__(self):
        pass
    
    def start(self):
        self.configure()

        exists = os.path.isfile("%s/children.pid" % (dir_path))
        if not exists:
            with open("%s/log/%s.log" % (dir_path,logfilename), "w") as logfile:

                command = "python3 -u monitor.py"
                pool = subprocess.Popen(command.split(),
                                                cwd="%s/agent" % dir_path,
                                                stdout=logfile,
                                                stderr=subprocess.STDOUT)

                with open("%s/children.pid" % (dir_path), "w") as pidfile:
                    pidfile.write(str(pool.pid)+"\n")

                    pidfile.close()

            print("Pool started successfully")
        else:
            print("Pool is already running.")
            print("If it is not, you should execute ./pool.py restart or ./pool.py clean then ./pool.py start")

    def stop(self):
        try:
            pidpath = "%s/children.pid" % (dir_path)
            exists = os.path.isfile(pidpath)
            if exists:
                with open(pidpath, "r") as pidfile:
                    pids = []
                    for pid in pidfile:
                        pids.append(pid)
                        intPid = int(pid)
                        try:
                            # print("Killing %d" % (intPid))
                            os.kill(intPid, signal.SIGKILL)
                        except Exception as e:
                            print(e)

                os.remove(pidpath)
                print("Pool was stopped successfully")
            else:
                print("Pool was already stopped")
            
        except Exception as e:
            print("Error stopping Pool: %s" % (e))

    def configure(self):
        pass

    def restart(self):
        self.stop()
        self.start()
    
    def clean(self):
        pass

if __name__ == '__main__':
    parameter_err = False

    if len(sys.argv) >= 2:
        action = sys.argv[1]

        pool = Pool()
        if action == "start":
            pool.start()
        elif action == "stop":
            pool.stop()
        elif action == "restart":
            pool.restart()
        elif action == "configure":
            pool.configure()
        else:
            parameter_err = True
    else:
        parameter_err = True
    
    if parameter_err:
        print("Please, enter a valid parameter (start, stop or restart)")
    
        