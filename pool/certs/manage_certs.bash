#!/bin/bash

CERTS_FOLDER=$(cd `dirname $0` && pwd)
OUTPUT_FOLDER="$CERTS_FOLDER/output"
SERVER_FOLDER="$CERTS_FOLDER/server_files"

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

if [ $# -eq 0 ]
then
    echo "What is your COPA Pool IP address?"
    read COPAPOOL
else
    if [ "$1" == "clear" ]
    then
        sudo rm $OUTPUT_FOLDER/*.crt $OUTPUT_FOLDER/*.key;
        exit 0
    fi

    COPAPOOL=$1
fi

if ! valid_ip $COPAPOOL
then
    echo "ERROR: Invalid IP Address!"
    exit 1
fi

sudo rm $OUTPUT_FOLDER/*.crt;
sudo sed -i "s/\(subjectAltName *= *IP:\).*/\1$COPAPOOL/" $CERTS_FOLDER/openssl.cnf;

echo "openssl req -x509 -new -nodes -key $SERVER_FOLDER/COPA-CA.key -sha256 -days 1024 -config $CERTS_FOLDER/openssl.cnf -out $OUTPUT_FOLDER/LXD-SERVER.crt -subj '/C=BR/ST=CE/O=GTEL/CN=COPA-CA'"
openssl req -x509 -new -nodes -key $SERVER_FOLDER/COPA-CA.key -sha256 -days 1024 -config $CERTS_FOLDER/openssl.cnf -out $OUTPUT_FOLDER/LXD-SERVER.crt -subj '/C=BR/ST=CE/O=GTEL/CN=COPA-CA'

cp $OUTPUT_FOLDER/LXD-SERVER.crt /usr/local/share/ca-certificates/
cp $OUTPUT_FOLDER/LXD-SERVER.crt /var/lib/lxd/server.crt #Replace LXD Certificates!
cp $SERVER_FOLDER/COPA-CA.key /var/lib/lxd/server.key #Replace LXD Certificates!

update-ca-certificates --fresh

service lxd restart

# Remove old certs
cd ~
lxc config trust list | grep COPA | awk '{print $2}' > removetempfile.txt
while read line; do lxc config trust remove $line; done < removetempfile.txt
rm removetempfile.txt

# Add new created cert
lxc config trust add $SERVER_FOLDER/lxd.crt