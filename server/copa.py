#! /usr/bin/python3
import os, sys, subprocess, signal
import datetime
import json
import re

dir_path = os.path.dirname(os.path.realpath(__file__))

logfilename = str(datetime.datetime.now().replace(microsecond=0))
webserver_config = "%s/webserver/copa/COPA_general.py" % (dir_path)
copa_path = "%s/.." % (dir_path)
servers_file = "%s/server/webserver/servers.txt" % (copa_path)

with open('%s/config.json' % (dir_path)) as json_data_file:
    CONFIG = json.load(json_data_file)

class COPA():
    def __init__(self, user="copa", password="copa"):
        self.user = user
        self.password = password
    
    def start(self):
        self.configure()

        exists = os.path.isfile("%s/children.pid" % (dir_path))
        if not exists:
            with open("%s/log/%s.log" % (dir_path,logfilename), "w") as logfile:

                command = "python3 -u manage.py runserver 0.0.0.0:8000"
                server = subprocess.Popen(command.split(),
                                                cwd="%s/webserver" % dir_path,
                                                stdout=logfile,
                                                stderr=subprocess.STDOUT)
                command = "python3 -u controller.py"
                monitor_control = subprocess.Popen(command.split(),
                                                cwd="%s/monitor" % dir_path,
                                                stdout=logfile,
                                                stderr=subprocess.STDOUT)
                
                command = "python3 -u manage.py shell < %s/webserver/core/refresh_service.py" % (dir_path)
                refresh_service = subprocess.Popen(command,
                                                shell=True,
                                                cwd="%s/webserver" % dir_path,
                                                stdout=logfile,
                                                stderr=subprocess.STDOUT)

                with open("%s/children.pid" % (dir_path), "w") as pidfile:
                    pidfile.write(str(server.pid)+"\n")
                    pidfile.write(str(monitor_control.pid)+"\n")
                    pidfile.write(str(refresh_service.pid)+"\n")

                    pidfile.close()

            print("COPA started successfully")
        else:
            print("COPA is already running.")
            print("If it is not, you should execute ./copa.py restart or ./copa.py clean then ./copa.py start")

    def stop(self):
        try:
            pidpath = "%s/children.pid" % (dir_path)
            exists = os.path.isfile(pidpath)
            if exists:
                with open(pidpath, "r") as pidfile:
                    pids = []
                    for pid in pidfile:
                        pids.append(pid)
                        intPid = int(pid)
                        try:
                            # print("Killing %d" % (intPid))
                            os.kill(intPid, signal.SIGKILL)
                        except Exception as e:
                            print(e)
                
                # Removing second process created by django
                command = "ps aux | grep -m1 'python3 manage.py' | awk '{print $2}'"
                django_pid = subprocess.check_output(command, shell=True).decode(sys.stdout.encoding)
                djangoIntPid = int(django_pid)
                try:
                    # print("Killing %d" % (djangoIntPid))
                    os.kill(djangoIntPid, signal.SIGKILL)
                except Exception as e:
                    print(e)


                os.remove(pidpath)
                print("COPA was stopped successfully")
            else:
                print("COPA was already stopped")
            
        except Exception as e:
            print("Error stopping COPA: %s" % (e))

    def configure(self):
        with open("%s" % (webserver_config), "r") as file:
            lines = []
            for line in file:
                line = re.sub('COPA_HOME = (.*)', r'COPA_HOME = "%s"' % (copa_path), line)
                line = re.sub('COPA_HOST = (.*)', r'COPA_HOST = "%s"' % (CONFIG['server_ip']), line)
                line = re.sub('IMAGE_HOST = (.*)', r'IMAGE_HOST = "%s"' % (CONFIG['server_ip']), line)
                lines.append(line)
            
        with open("%s" % (webserver_config), "w") as file:
            file.writelines(lines)

        servers = open(servers_file, "w+")

        for pool in CONFIG["pools"]:
            servers.write("%s;%s:8443" % (pool["label"], pool["ip"])+"\n")

        servers.close()

    def restart(self):
        self.stop()
        self.start()
    
    def manage_certs(self):
        print("Functionality under construction")
        return None
        
        self.configure()
        command = "python3 -u automated_cert.py"
        manage_certs = subprocess.Popen(command.split(),
                                        cwd="%s/certs" % dir_path,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT)
        
        while True:
            line = manage_certs.stdout.readline().decode(sys.stdout.encoding).replace("\n","")
            print(line)
            if not line: break

    def clean(self):
        pass

if __name__ == '__main__':
    parameter_err = False

    if len(sys.argv) >= 2:
        action = sys.argv[1]

        copa = COPA()
        if action == "start":
            copa.start()
        elif action == "stop":
            copa.stop()
        elif action == "restart":
            copa.restart()
        elif action == "configure":
            copa.configure()
        elif action == "managecerts":
            copa.configure()
            copa.manage_certs()
        else:
            parameter_err = True
    else:
        parameter_err = True
    
    if parameter_err:
        print("Please, enter a valid parameter (start, stop or restart)")
    
        