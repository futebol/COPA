#!/bin/bash

CERTS_FOLDER=$(cd `dirname $0` && pwd)
OUTPUT_FOLDER="$CERTS_FOLDER/output"

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

if [ $# -eq 0 ]
then
    echo "What is your COPA Server IP address?"
    read COPASERVER
else
    if [ "$1" == "clear" ]
    then
        sudo rm $OUTPUT_FOLDER/*.crt $OUTPUT_FOLDER/*.key;
        exit 0
    fi

    COPASERVER=$1
fi

if ! valid_ip $COPASERVER
then
    echo "ERROR: Invalid IP Address!"
    exit 1
fi

sudo rm $OUTPUT_FOLDER/*.crt $OUTPUT_FOLDER/*.key;
sudo sed -i "s/\(subjectAltName *= *IP:\).*/\1$COPASERVER/" $CERTS_FOLDER/openssl.cnf;

# Make Certificate Authority
openssl genrsa -out $OUTPUT_FOLDER/COPA-CA.key 2048

cp $OUTPUT_FOLDER/COPA-CA.key $OUTPUT_FOLDER/lxd.key

openssl req -x509 -new -nodes -key $OUTPUT_FOLDER/COPA-CA.key -sha256 -days 1024 -config $CERTS_FOLDER/openssl.cnf -out $OUTPUT_FOLDER/COPA-CA.crt -subj '/C=BR/ST=CE/O=GTEL/CN=COPA-CA' #

cp $OUTPUT_FOLDER/COPA-CA.crt /usr/local/share/ca-certificates/
cp $OUTPUT_FOLDER/COPA-CA.crt /var/lib/lxd/server.crt
cp $OUTPUT_FOLDER/lxd.key /var/lib/lxd/server.key

# Make Certificates
openssl req -newkey rsa:2048 -nodes -config $CERTS_FOLDER/openssl.cnf -out $OUTPUT_FOLDER/lxd.csr -subj '/C=BR/ST=CE/O=GTEL/CN=COPA'

openssl x509 -signkey $OUTPUT_FOLDER/COPA-CA.key -in $OUTPUT_FOLDER/lxd.csr -req -days 365 -out $OUTPUT_FOLDER/lxd.crt

rm $OUTPUT_FOLDER/lxd.csr privkey.pem

cp $OUTPUT_FOLDER/COPA-CA.crt /usr/local/share/ca-certificates/
cp $OUTPUT_FOLDER/COPA-CA.key $OUTPUT_FOLDER/lxd.key

update-ca-certificates --fresh

service lxd restart