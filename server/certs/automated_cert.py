#!/usr/bin/python3
#
# This script aims to automate the management of LXD certificates automatically
# 1. In the first moment we will gather the IPs from the server.txt file
# 2. Later on, we will get the IPs from the COPA API
# 
# by Henrique Resende

import os, subprocess, sys

dir_path = os.path.dirname(os.path.realpath(__file__))
class Server():
    def __init__(self, ip=None):
        self.ip = self.getIpFromConfig() if ip == None else ip
        self.user = "copa"
        self.password = "copa"
        self.dst_root_path = '/copa'
        self.dst_cert_folder = '%s/server/certs' % (self.dst_root_path)

    def shellCommand(self, command):
        log = subprocess.check_output(command, shell=True).decode(sys.stdout.encoding)

        return log

    def getIpFromConfig(self):
        file_path = "%s/../webserver/copa/COPA_general.py" % (dir_path)

        command = "cat %s | grep COPA_HOST | awk '{print $3}' | sed 's/\"//g'" % (file_path)
        
        return self.shellCommand(command)

    def getIp(self):
        return self.ip

    def reOwnFiles(self):
        print("Started reowning files")
        command = "echo %s | sudo -S chown -R copa:copa %s/output/*" % (self.password, self.dst_cert_folder)

        log = self.shellCommand(command)

        print(log)
        print("Finished reowning files")

    def make_certs(self):
        print("Creating servers certs")

        file_path = "%s/manage_certs.bash" % (dir_path)

        command = "echo %s | sudo -S %s %s" % \
            (self.password, file_path, self.ip)

        log = self.shellCommand(command)

        print(log)

        self.reOwnFiles()
        print("Started reowning files")

    def getPools(self):
        #pwd
        file_path = "%s/../webserver/servers.txt" % (dir_path)

        #read servers.txt
        pools = []
        servers_file = open(file_path, 'r')

        line = servers_file.readline()

        while line:
            # Example format
            # Server1;10.0.2.9:8443
            line = line.split(";")
            line[1] = line[1].split(":")

            label = line[0]
            ip = line[1][0]
            port = line[1][1]

            pool_data = {
                'label': label,
                'ip': ip,
                'port': port
            }

            pools.append(Pool(pool_data['ip']))

            line = servers_file.readline()

        return pools

class Pool():
    def __init__(self, ip, port=22):
        self.ip = ip
        self.port = port

        # Default
        self.user = 'copa'
        self.password = 'copa'
        self.src_path = '%s/output' % (dir_path)
        self.dst_root_path = '/copa'
        self.dst_path = '%s/pool/certs/server_files' % (self.dst_root_path)
        self.dst_cert_folder = '%s/pool/certs' % (self.dst_root_path)

    def setSSHCredentials(self, user, password):
        self.user = user
        self.password = password

        return {
            'user': self.user,
            'password': self.password
        }
    
    def getSSHCredentials(self):
        return {
            'user': self.user,
            'password': self.password
        }

    def deploySSHCommand(self, command):
        ssh_shell = "sshpass -p %s ssh -o 'UserKnownHostsFile=/dev/null' -o 'StrictHostKeyChecking=no' %s@%s -p %s '%s'" % \
            (self.password, self.user, self.ip, self.port, command)

        log = subprocess.check_output(ssh_shell, shell=True).decode(sys.stdout.encoding)

        return log
    
    def transfer_certs(self):
        print("Transferring files to %s" % (self.ip))

        command = "sshpass -p %s scp -o 'UserKnownHostsFile=/dev/null' -o 'StrictHostKeyChecking=no' -P %s %s/* %s@%s:%s" % \
            (self.password, self.port, self.src_path, self.user, self.ip, self.dst_path)

        log = subprocess.check_output(command, shell=True).decode(sys.stdout.encoding)

        print(log)

    def remove_old_certs(self):
        print("Removing old certs in %s" % (self.ip))

        command = "cd ~;\
            lxc config trust list | grep COPA | awk '{print \$2}' > removetempfile.txt;\
            while read line; do lxc config trust remove \$line; done < removetempfile.txt;\
            rm removetempfile.txt;"

        log = self.deploySSHCommand(command)

        print(log)

    def make_certs(self):
        print("Creating certs in %s" % (self.ip))

        command = "echo %s | sudo -S %s/manage_certs.bash %s" % \
            (self.password, self.dst_cert_folder, self.ip)

        log = self.deploySSHCommand(command)

        print(log)

    def createOrUpdateRepository(self, label, ip):
        self.removeRepository(label)
        self.createRepository(label, ip)
    
    def removeRepository(self, label):
        print("Removing repository %s" % (label))

        command = "lxc remote remove %s" % (label)

        self.deploySSHCommand(command)
    
    def createRepository(self, label, ip):
        print("Adding repository %s" % (label))

        command = "lxc remote add %s %s" % (label, ip)

        self.deploySSHCommand(command)


if __name__ == '__main__':
    # Execute manage certs in Server
    server = Server()
    pools = server.getPools()

    if pools:
        server.make_certs()
    
        # Transfer to pools & execute manage certs in Pools
        for pool in pools:
            pool.transfer_certs()
            pool.make_certs()
            # pool.createOrUpdateRepository("copa_repo", server.getIp())
    
    

