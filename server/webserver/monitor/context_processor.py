from core.models import Pool

def menuvars(request):
    pools = list(Pool.objects.all().values("name"))

    return {
        'pools_names': pools
    }