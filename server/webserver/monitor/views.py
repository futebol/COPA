from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views import generic
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from core.models import Configuration, TierClass, Pool, Container
from monitor.models import KPILink, KPIResources, KPIWireless


# Create your views here.
def index(request):
    try:
        return redirect(Configuration.objects.get(name="default"))
    except ObjectDoesNotExist:
        return redirect("/monitor/configuration/create/")

def dashboard(request):
    pools = list(Pool.objects.all().values())

    response = dict()
    response['pools'] = list()
    total_pools = 0
    total_containers = 0
    total_resources = 0

    for pool in pools:
        containers = list(Container.objects \
                        .filter(pool=pool["name"]) \
                        .values("name", "status"))

        pool['containers'] = containers
        pool['id'] = pool["name"].replace(" ","").lower()

        response['pools'].append(pool)
        total_containers = total_containers + len(containers)

    total_pools = len(pools)

    response['overview'] = {
        'pools': total_pools,
        'containers': total_containers,
        'resources': total_resources
    }

    return render(
        request,
        'dashboard.html',
        response
    )

def about(request):
    return render(request, 'about.html')

def pool(request, name):
    pool = Pool.objects.get(name=name)
    containers = list(Container.objects \
                    .filter(pool=name) \
                    .values("name", "status"))
    neighbors = list()
    neighborsl1 = list(
                        KPILink.objects.filter(locus1=name)
                        .order_by("-timestamp")
                        .values_list("locus2", flat=True).distinct()
    )
    
    neighborsl2 = list(
                        KPILink.objects.filter(locus2=name)
                        .order_by("-timestamp")
                        .values_list("locus1", flat=True).distinct()
    )

    neighbors.extend( x for x in neighborsl1 if x not in neighbors)
    neighbors.extend( x for x in neighborsl2 if x not in neighbors)

    # Removing duplicates
    # neighbors = neighborsl1 + list(set(neighborsl2) - set(neighborsl1))

    response = {
        'pool': pool,
        'containers': containers,
        'neighbors': neighbors,
    }

    return render(
        request,
        'pool.html',
        response
    )

class ConfigurationDetailView(generic.DetailView):
    model = Configuration

    def get_queryset(self):
        return Configuration.objects.all()


class ConfigurationCreate(CreateView):
    model = Configuration
    fields = "__all__"


class ConfigurationUpdate(UpdateView):
    model = Configuration
    fields = "__all__"


class ConfigurationDelete(DeleteView):
    model = Configuration
    success_url = reverse_lazy("monitor_index")


class TierClassDetailView(generic.DetailView):
    model = TierClass


class TierClassCreate(CreateView):
    model = TierClass
    fields = "__all__"


class TierClassUpdate(UpdateView):
    model = TierClass
    fields = "__all__"


class TierClassDelete(DeleteView):
    model = TierClass
    success_url = reverse_lazy("monitor_index")


class LocusDetailView(generic.DetailView):
    model = Pool


class LocusCreate(CreateView):
    model = Pool
    fields = "__all__"

    def get_initial(self):
        if "pk" in self.kwargs:
            tier = TierClass.objects.get(pk=self.kwargs.get("pk"))
            return {"tier_class": tier}


class LocusUpdate(UpdateView):
    model = Pool
    fields = "__all__"


class LocusDelete(DeleteView):
    model = Pool
    success_url = reverse_lazy("monitor_index")
