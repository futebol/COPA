from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from core.models import Configuration, Pool, Container
from ..models import KPICommand, KPILink, KPIResources,\
                    KPIWireless, KPILte, KPIOptical

from datetime import datetime, timedelta
from django.db.models import Avg, Q, F, Max

import json

class RestFunctions(object):
    @staticmethod
    def check_required_fields(required_fields=None, provided_fields=None):
        for requirement in required_fields:
            if requirement not in provided_fields:
                raise Exception("Required Field '{}' not in"
                                " post".format(requirement))

    @staticmethod
    def locus(request):
        """Request list of Tiers from an Configuration.
        """
        response = dict()
        if request.method == "GET":
            response["locus"] = list(
                    Pool.objects.all().values_list("name", flat=True))
            response["success"] = "locus"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)

    @staticmethod
    @csrf_exempt
    def dashboard(request):
        """KPI measurements.
        """
        response = dict()

        if request.method == "GET":

            try:
                pools = list(Pool.objects.all().values())

                response["data"] = list()
                # return JsonResponse(response)
            except ObjectDoesNotExist:
                return JsonResponse({"error": "Pool not registered"})

            try:
                time_threshold = datetime.now() - timedelta(seconds=30)
                #time_threshold = 0

                for pool in pools:
                    cpu = KPIResources.objects \
                        .filter(locus=pool["name"], ) \
                        .values_list('timestamp', 'CPU')
                    cpu = list(cpu)

                    memory = KPIResources.objects \
                        .filter(locus=pool["name"], ) \
                        .values_list('timestamp', 'memory')
                    memory = list(memory)

                    containers = Container.objects \
                        .filter(pool=pool["name"]) \
                        .values("name", "status")
                    containers = list(containers)

                    throughput = KPILink.objects \
                        .filter(locus1=pool["name"], ) \
                        .values_list('timestamp', 'throughput')
                    throughput = list(throughput)

                    # if throughput["throughput"]:
                    #     throughput["throughput"] = float(throughput["throughput"])*1024

                    response["data"].append({
                        "name": pool["name"],
                        "cpu": cpu,
                        "mem": memory,
                        "throughput": throughput,
                        "containers": containers
                    })

            except Exception as e:
                return JsonResponse({"error": str(e)})

            response["success"] = "dashboard"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)

    @staticmethod
    @csrf_exempt
    def pool(request, name):
        """KPI measurements.
        """
        response = dict()

        if request.method == "GET":
            try:
                pool =  Pool.objects.get(name=name)

            except ObjectDoesNotExist:
                return JsonResponse({"error": "Pool not registered"})

            try:
                if request.GET["timeInterval"]:
                    timeInterval = int(request.GET["timeInterval"])
                else:
                    timeInterval = 0

                if timeInterval != 0:
                    time_threshold = datetime.now() - timedelta(minutes=timeInterval)
                else:
                    time_threshold = datetime.now() - timedelta(days=1)

                #time_threshold = 0

                cpu = KPIResources.objects \
                    .filter(locus=name, timestamp__gt=time_threshold) \
                    .values_list('timestamp', 'CPU')
                cpu = list(cpu)

                memory = KPIResources.objects \
                    .filter(locus=name, timestamp__gt=time_threshold) \
                    .values_list('timestamp', 'memory')
                memory = list(memory)

                containers = Container.objects \
                    .filter(pool=name) \
                    .values("name", "status")
                containers = list(containers)

                throughput = KPILink.objects \
                    .filter(locus1=name, timestamp__gt=time_threshold) \
                    .values_list('timestamp', 'throughput')
                throughput = list(throughput)

                latency = KPILink.objects \
                    .filter((Q(locus1=name) | Q(locus2=name)), timestamp__gt=time_threshold) \
                    .values_list('timestamp', 'latency_median_2to1', 'latency_median_1to2', 'locus1', 'locus2')
                latency = list(latency)

                latencies = {}

                for data in latency:
                    if data[3] == name:
                        neighbor = data[4]
                    else:
                        neighbor = data[3]
                    
                    if neighbor not in latencies:
                        latencies[neighbor] = []
                    
                    latencies[neighbor].append([
                        data[0],
                        data[1]+data[2]
                    ])

                # latency_aux = []
                # for x in latency:
                #     latency_aux.append([
                #         x[0],
                #         x[1]+x[2]
                #     ])

                jitter = KPILink.objects \
                    .filter((Q(locus1=name) | Q(locus2=name)), timestamp__gt=time_threshold) \
                    .values_list('timestamp', 'jitter_2to1', 'locus1', 'locus2')
                jitter = list(jitter)

                jitters = {}

                for data in jitter:
                    if data[2] == name:
                        neighbor = data[3]
                    else:
                        neighbor = data[2]
                    
                    if neighbor not in jitters:
                        jitters[neighbor] = []
                    
                    jitters[neighbor].append([
                        data[0],
                        data[1]
                    ])

                rntis = list(KPILte.objects.order_by().values("rnti").distinct())

                ltes = {}
                for rnti in rntis:
                    # device_data = KPILte.objects.filter(Q(locus=name) & Q(rnti=rnti["rnti"]), timestamp__gt=lte_time_threshold).values()
                    rnti = rnti["rnti"]
                    device_data = KPILte.objects \
                                    .filter((Q(locus=name) & Q(rnti=rnti)), timestamp__gt=time_threshold) \
                                    .values_list("timestamp", "ul_snr") 
                    device_data = list(device_data)
                    # ltes.append(device_data)
                    ltes[rnti] = device_data

                funcs = list(KPIOptical.objects.order_by().values("func_name").distinct())

                opticals = {}
                for func in funcs:
                    func = func["func_name"]
                    func_data = KPIOptical.objects \
                                .filter((Q(locus=name) & Q(func_name=func)), timestamp__gt=time_threshold) \
                                .values_list("timestamp", "total")
                    func_data = list(func_data)
                    # opticals.append(func_data)
                    opticals[func] = func_data

                # if throughput["throughput"]:
                #     throughput["throughput"] = float(throughput["throughput"])*1024

                response = {
                    "name": name,
                    "cpu": cpu,
                    "mem": memory,
                    "throughput": throughput,
                    "latency": latencies,
                    "jitter": jitters,
                    "ltes": ltes,
                    "opticals": opticals,
                    "containers": containers
                }

            except Exception as e:
                return JsonResponse({"error": str(e)})

            response["success"] = "dashboard"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)

    @staticmethod
    @csrf_exempt
    def kpilink(request):
        """KPI measurements.
        """
        response = dict()
        if request.method == "GET":

            try:
                required_fields = ["locus"]
                RestFunctions.check_required_fields(required_fields,
                                                    request.GET)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.GET["locus"])
            except ObjectDoesNotExist:
                return JsonResponse({"error": "Pool not registered"})

            try:
                response["measurements"] = list(
                        KPILink.objects.filter(locus1=locus)
                        .order_by("-timestamp")
                        .values())
            except Exception as e:
                return JsonResponse({"error": str(e)})

            if "last" in request.GET and request.GET["last"] != "":
                last_records = int(request.GET["last"])
                try:
                    response["measurements"] = response["measurements"][
                                               :int(last_records)]
                except Exception as e:
                    return JsonResponse({"error": str(e)})

            response["success"] = "kpilink"

        elif request.method == "POST":
            try:
                required_fields = ["locus1", "locus2", "jitter_1to2",
                                   "latency_max_1to2", "latency_median_1to2",
                                   "latency_min_1to2", "jitter_2to1",
                                   "latency_max_2to1", "latency_median_2to1",
                                   "latency_min_2to1", "throughput"]

                RestFunctions.check_required_fields(required_fields,
                                                    request.POST)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus1 = Pool.objects.get(name=request.POST["locus1"])
                locus2 = Pool.objects.get(name=request.POST["locus2"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                KPILink(locus1=locus1,
                        locus2=locus2,
                        jitter_1to2=request.POST["jitter_1to2"],
                        latency_max_1to2=request.POST["latency_max_1to2"],
                        latency_median_1to2=request.POST["latency_median_1to2"],
                        latency_min_1to2=request.POST["latency_min_1to2"],
                        jitter_2to1=request.POST["jitter_2to1"],
                        latency_max_2to1=request.POST["latency_max_2to1"],
                        latency_median_2to1=request.POST["latency_median_2to1"],
                        latency_min_2to1=request.POST["latency_min_2to1"],
                        throughput=request.POST["throughput"]).save()
            except Exception as e:
                response["error"] = str(e)

            response["success"] = "post_kpilink"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)

    # KPI COMMAND
    @staticmethod
    @csrf_exempt
    def kpicommand(request):
        """Command measurements
        """
        response = dict()
        if request.method == "GET":
            try:
                required_fields = ["locus"]
                RestFunctions.check_required_fields(required_fields,
                                                    request.GET)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.GET["locus"])
            except Exception as e:
                return JsonResponse({"Error": str(e)})

            try:
                response["measurements"] = list(
                        KPICommand.objects.filter(locus=locus)
                        .order_by("-timestamp")
                        .values())
            except Exception as e:
                return JsonResponse({"error": str(e)})

            if "last" in request.GET and request.GET["last"] != "":
                last_records = request.GET["last"]
                try:
                    response["measurements"] = response["measurements"][
                                               :int(last_records)]
                except Exception as e:
                    return JsonResponse({"error": str(e)})

            response["success"] = "kpicommand"

        elif request.method == "POST":
            try:
                required_fields = ["locus", "cmd", "cmd_type",
                                   "proc_time", "response_time"]
                RestFunctions.check_required_fields(required_fields,
                                                    request.POST)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.POST["locus"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                KPICommand(locus=locus,
                           proc_time=request.POST["proc_time"],
                           response_time=request.POST["response_time"],
                           cmd=request.POST["cmd"],
                           cmd_type=request.POST["cmd_type"]).save()
            except Exception as e:
                response["error"] = str(e)

            response["success"] = "kpicommand"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)

    # KPI RESOURCE
    @staticmethod
    @csrf_exempt
    def kpiresource(request):
        """Request command measurements (Response time) from a Tier
        """
        response = dict()
        if request.method == "GET":
            try:
                required_fields = ["locus"]
                RestFunctions.check_required_fields(required_fields,
                                                    request.GET)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.GET["locus"])
            except Exception as e:
                return JsonResponse({"Error": str(e)})

            try:
                response["measurements"] = list(
                        KPIResources.objects.filter(locus=locus)
                        .order_by("-timestamp")
                        .values())
            except Exception as e:
                return JsonResponse({"error": str(e)})

            if "last" in request.GET and request.GET["last"] != "":
                last_records = request.GET["last"]
                try:
                    response["measurements"] = response["measurements"][
                                               :int(last_records)]
                except Exception as e:
                    return JsonResponse({"error": str(e)})

            response["success"] = "kpiresource"

        elif request.method == "POST":
            try:
                required_fields = ["locus", "CPU", "memory"]
                RestFunctions.check_required_fields(required_fields,
                                                    request.POST)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.POST["locus"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                KPIResources(locus=locus,
                             CPU=request.POST["CPU"],
                             memory=request.POST["memory"]).save()
            except Exception as e:
                response["error"] = str(e)

            response["success"] = "kpiresource"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)

    # KPI WIRELESS
    @staticmethod
    @csrf_exempt
    def kpiwireless(request):
        """Wireless KPI measurements
        """
        response = dict()
        if request.method == "GET":
            try:
                required_fields = ["locus"]
                RestFunctions.check_required_fields(required_fields,
                                                    request.GET)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.GET["locus"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                response["measurements"] = list(
                        KPIWireless.objects.filter(locus=locus)
                        .order_by("-timestamp")
                        .values())
            except Exception as e:
                return JsonResponse({"error": str(e)})

            if "last" in request.GET and request.GET["last"] != "":
                last_records = request.GET["last"]
                try:
                    response["measurements"] = response["measurements"][
                                               :int(last_records)]
                except Exception as e:
                    return JsonResponse({"error": str(e)})

            response["success"] = "kpiwireless"

        elif request.method == "POST":
            register_array = json.loads(request.body.decode("utf-8"))
            
            try:
                required_fields = ["locus", "mac", "mfb", "tdls", "wmm",
                                   "authenticated", "authorized",
                                   "expected_throughput", "inactive_time",
                                   "preamble", "rx_bitrate", "rx_bytes",
                                   "rx_packets", "signal", "signal_avg",
                                   "tx_bitrate", "tx_bytes", "tx_failed",
                                   "tx_retries"]

                for register in register_array:
                    RestFunctions.check_required_fields(required_fields,
                                                        register)
                                                        
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=register_array[0]["locus"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                for register in register_array:
                    KPIWireless(locus=locus,
                                mac=register["mac"],
                                mfb=register["mfb"],
                                tdls=register["tdls"],
                                wmm=register["wmm"],
                                authenticated=register["authenticated"],
                                authorized=register["authorized"],
                                expected_throughput=register[
                                    "expected_throughput"],
                                inactive_time=register["inactive_time"],
                                preamble=register["preamble"],
                                rx_bitrate=register["rx_bitrate"],
                                rx_bytes=register["rx_bytes"],
                                rx_packets=register["rx_packets"],
                                signal=register["signal"],
                                signal_avg=register["signal_avg"],
                                tx_bitrate=register["tx_bitrate"],
                                tx_bytes=register["tx_bytes"],
                                tx_failed=register["tx_failed"],
                                tx_retries=register["tx_retries"]).save()
                                
            except Exception as e:
                response["error"] = str(e)

            response["success"] = "post_kpiwireless"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)


    @staticmethod
    @csrf_exempt
    def kpilte(request):
        """LTE KPI measurements
        """
        response = dict()
        if request.method == "GET":
            try:
                required_fields = ["locus"]
                RestFunctions.check_required_fields(required_fields,
                                                    request.GET)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.GET["locus"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                response["measurements"] = list(
                        KPILte.objects.filter(locus=locus)
                        .order_by("-timestamp")
                        .values())
            except Exception as e:
                return JsonResponse({"error": str(e)})

            if "last" in request.GET and request.GET["last"] != "":
                last_records = request.GET["last"]
                try:
                    response["measurements"] = response["measurements"][
                                               :int(last_records)]
                except Exception as e:
                    return JsonResponse({"error": str(e)})

            response["success"] = "kpilte"

        elif request.method == "POST":            
            try:
                required_fields = ["locus", "dl_ri", "dl_bler", "dl_mcs", "dl_cqi",
                                   "dl_brate", "ul_bler", "ul_mcs", "ul_snr", "ul_brate", 
                                   "ul_bsr", "ul_phr", "tx_gain", "rx_gain", "rnti"]

                RestFunctions.check_required_fields(required_fields,
                                                    request.POST)
                                            
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.POST["locus"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                KPILte(locus=locus,
                        dl_ri=request.POST["dl_ri"],
                        dl_bler=request.POST["dl_bler"],
                        dl_mcs=request.POST["dl_mcs"],
                        dl_cqi=request.POST["dl_cqi"],
                        dl_brate=request.POST["dl_brate"],
                        ul_bler=request.POST["ul_mcs"],
                        ul_snr=request.POST["ul_snr"],
                        ul_brate=request.POST["ul_brate"],
                        ul_bsr=request.POST["ul_bsr"],
                        ul_phr=request.POST["ul_phr"],
                        tx_gain=request.POST["tx_gain"],
                        rx_gain=request.POST["rx_gain"],
                        rnti=request.POST["rnti"]
                        ).save()
                            
            except Exception as e:
                response["error"] = str(e)

            response["success"] = "post_kpilte"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)

    @staticmethod
    @csrf_exempt
    def kpioptical(request):
        """Optical KPI measurements
        """
        response = dict()
        if request.method == "GET":
            try:
                required_fields = ["locus"]
                RestFunctions.check_required_fields(required_fields,
                                                    request.GET)
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.GET["locus"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                response["measurements"] = list(
                        KPIOptical.objects.filter(locus=locus)
                        .order_by("-timestamp")
                        .values())
            except Exception as e:
                return JsonResponse({"error": str(e)})

            if "last" in request.GET and request.GET["last"] != "":
                last_records = request.GET["last"]
                try:
                    response["measurements"] = response["measurements"][
                                               :int(last_records)]
                except Exception as e:
                    return JsonResponse({"error": str(e)})

            response["success"] = "kpilte"

        elif request.method == "POST":            
            try:
                required_fields = ["locus", "func_name", "port_number", 
                                    "in", "out", "total"]

                RestFunctions.check_required_fields(required_fields,
                                                    request.POST)
                                            
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                locus = Pool.objects.get(name=request.POST["locus"])
            except Exception as e:
                return JsonResponse({"error": str(e)})

            try:
                KPIOptical(locus=locus,
                        func_name=request.POST["func_name"],
                        port_number=request.POST["port_number"],
                        bits_in=request.POST["in"],
                        bits_out=request.POST["out"],
                        total=request.POST["total"]
                        ).save()
                            
            except Exception as e:
                response["error"] = str(e)

            response["success"] = "post_kpioptical"

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)

    @staticmethod
    @csrf_exempt
    def update_configuration(request):
        response = dict()
        if request.method == "POST":
            try:
                exp = Configuration.objects.get(name="default")
            except Exception as e:
                return JsonResponse({"error": str(e)})

            update_fields = []
            if "delegation_mode" in request.POST:
                new_delegation_mode = request.POST["delegation_mode"]
                if exp.delegation_mode != new_delegation_mode:
                    exp.delegation_mode = new_delegation_mode
                    update_fields += ["delegation_mode"]

            if "auto_delegation_type" in request.POST:
                new_auto_delegation_type = request.POST["auto_delegation_type"]
                if exp.auto_delegation_type != new_auto_delegation_type:
                    exp.auto_delegation_type = new_auto_delegation_type
                    update_fields += ["auto_delegation_type"]

            try:
                exp.save(update_fields=update_fields)
            except Exception as e:
                response["error"] = str(e)

            response["success"] = "update_configuration"

        elif request.method == "GET":  # TO BE TESTED!
            config = list(Configuration.objects.filter(name="default").values())
            response["configuration"] = config

        else:
            response["error"] = "Method not allowed"

        return JsonResponse(response)
