
/**
 * Charts id's root: chart
 * Chart metrics: cpu, ram, net, rtt, jitter
 */

var charts;
var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];
var pool = last_part;
var pollTime = 3000;


$(document).ready(function(){
    var root = 'chart';
    
    charts = { 
        "cpu": ($("#"+root+"-cpu").length != 0) ? Highcharts.chart(root+"-cpu", cpu_options) : null,
        "ram": ($("#"+root+"-ram").length != 0) ? Highcharts.chart(root+"-ram", ram_options) : null,
        "net": ($("#"+root+"-net").length != 0) ? Highcharts.chart(root+"-net", net_options) : null,
        "rtt": ($("#"+root+"-rtt").length != 0) ? Highcharts.chart(root+"-rtt", rtt_options) : null,
        "jitter": ($("#"+root+"-jitter").length != 0) ? Highcharts.chart(root+"-jitter", jit_options) : null,
        "lte": ($("#"+root+"-lte").length != 0) ? Highcharts.chart(root+"-lte", lte_options) : null,
        "optical": ($("#"+root+"-optical").length != 0) ? Highcharts.chart(root+"-optical", opt_options) : null,
    };

    containerButtons();
    migrateButton();
    createContainerButton();

    postDashboard();
});

function removeContainer(name){
    containerLine = $("tr[container="+name+"]");
    list = $(containerLine).parent();
    childrenLength = $(list).children().length;

    $(containerLine).remove();

    if(childrenLength == 1){
        tbody = list;
        table = $(tbody).parent();
        divTable = $(table).parent();
        boxbody = $(divTable).parent();

        $(divTable).remove();
        $(boxbody).html("No containers")
    }

}
function updateContainer( name, operation){
    cntr_tr = $("tr[container="+name+"]");
    btns = $(cntr_tr).find(".cntr-btns");

    if(operation == "start" || operation == "stop" || operation=="migrate"){
        if(operation == "start"){
            newLabel = "Running";
            newclass = "label-success";
            removeclass = "label-danger label-info";

            $(btns).find("a[operation=start]").addClass("hide");
            $(btns).find("a[operation=stop]").removeClass("hide");
            $(btns).find("a[operation=delete]").addClass("hide");
            $(btns).find("a[operation=migrate]").removeClass("hide");
        }
        else if(operation == "migrate"){
            newLabel = "Migrating";
            newclass = "label-info";
            removeclass = "label-danger label-success";
    
            $(btns).find("a[operation=start]").addClass("hide");
            $(btns).find("a[operation=delete]").addClass("hide");
            $(btns).find("a[operation=stop]").addClass("hide");
            $(btns).find("a[operation=migrate]").addClass("hide");
        }
        else{
            newLabel = "Stopped";
            newclass = "label-danger";
            removeclass = "label-success label-info";

            $(btns).find("a[operation=start]").removeClass("hide");
            $(btns).find("a[operation=delete]").removeClass("hide");
            $(btns).find("a[operation=stop]").addClass("hide");
            $(btns).find("a[operation=migrate]").removeClass("hide");
        }

        $("tr[container="+name+"]").find(".label").removeClass(removeclass);
        $("tr[container="+name+"]").find(".label").addClass(newclass);
        $("tr[container="+name+"]").find(".label").html(newLabel);
    }
    else if(operation == "delete"){
        removeContainer(name);
    }
    else{
        console.log("Container operation not identified");
    }
    
}
function createContainerButton(){
    $(".cntr-create").click(function(){
        var pool = $(this).attr("pool");

        $("#modal-cntr-images").children().remove();
        $(".cntr-create-submit").attr("pool", pool);
        console.log("Send image request");
        $.ajax({
            url: "/REST/image",
            dataType: "json",
            method: "GET",
            success: function(result){
                console.log(result);
                if(result.hasOwnProperty('success')){
                    images = result.images;

                    for(image in images){
                        option = $("<option>").html(image);
                        $("#modal-cntr-images").append(option);
                    }

                }
                else if(result.hasOwnProperty('error')){
                    console.log("Error: "+result.error);
                    setAlert("error", "Error", result.error);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });

    $(".cntr-create-submit").click(function(){
        operation = "create";
        container = $("#modal-cntr-name").val();
        image = $("#modal-cntr-images option:selected").first().text();
        pool = $(this).attr("pool");

        console.log("Operation: "+operation);
        console.log("Container: "+container);
        console.log("Image: "+image);
        console.log("Pool: "+pool);

        $.ajax({
            url: "/REST/container",
            dataType: "json",
            method: "POST",
            data: {
                operation: operation,
                container_name: container,
                container_pool: pool,
                image_type: image
            },
            success: function(result){
                console.log(result);
                if(result.hasOwnProperty('success')){
                    removeContainer(container);
                    setAlert("success", "Success", result.success);

                }
                else if(result.hasOwnProperty('error')){
                    console.log("Error: "+result.error);
                    setAlert("error", "Error", result.error);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        })
    });
}
function migrateButton(){
    $(".cntr-migrate").click(function(){
        var tr = $(this).parent().parent();
        var pool = $(tr).attr("pool");
        var container = $(tr).attr("container");
        var oldstatus = $(tr).find(".cntr-status").first().html();

        $("#dest-pools").attr("source_pool", pool);
        $("#dest-pools").attr("container", container);
        $("#dest-pools").attr("old_status", oldstatus);

        console.log("Migrations setup:");
        console.log("Source "+pool);
        console.log("Container "+container);
    });

    $(".cntr-migrate-dest").click(function(){
        var source_pool = $("#dest-pools").attr("source_pool");
        var container = $("#dest-pools").attr("container");
        var operation = "migrate";
        var oldstatus = $("#dest-pools").attr("old_status");
        var dest_pool = $(this).attr("value");

        if(oldstatus == "Running"){
            oldoperation = "start";
        }
        else {
            oldoperation = "stop";
        }

        console.log("Migrating...");
        console.log("Old status: "+oldstatus);
        $("#modal-migration").find("button[class=close]").click();
        updateContainer(container, operation);

        $.ajax({
            url: "/REST/container",
            dataType: "json",
            method: "POST",
            data: {
                operation: operation,
                container_name: container,
                container_pool: source_pool,
                destination_pool: dest_pool
            },
            success: function(result){
                console.log(result);
                if(result.hasOwnProperty('success')){
                    removeContainer(container);
                    setAlert("success", "Success", result.success);

                }
                else if(result.hasOwnProperty('error')){
                    console.log("Error: "+result.error);
                    setAlert("error", "Error", result.error);
                    updateContainer(container, oldoperation);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                updateContainer(container, oldoperation);
            }
        });
    });
}
function containerButtons(){
    $(".cntr-operation").click(function(){
        var tr = $(this).parent().parent();
        var pool = $(tr).attr("pool");
        var container = $(tr).attr("container");
        var operation = $(this).attr("operation");

        console.log("Pool: "+pool);
        console.log("Container: "+container);
        console.log("Operation: "+operation);

        $.ajax({
            url: "/REST/container",
            dataType: "json",
            method: "POST",
            data: {
                operation: operation,
                container_name: container,
                container_pool: pool
            },
            success: function(result){
                console.log(result);
                if(result.hasOwnProperty('success')){
                    updateContainer(container,operation);
                    setAlert("success", "Success", result.success);

                }
                else if(result.hasOwnProperty('error')){
                    console.log("Error: "+result.error);
                    setAlert("error", "Error", result.error);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });

    });
}

function fixDate4Chart(mydata, neighbor){
    for(var j=0; j < mydata.cpu.length; j++){
        mydata.cpu[j][0] = new Date(mydata.cpu[j][0]).getTime();
    }

    for(var j=0; j < mydata.mem.length; j++){
        mydata.mem[j][0] = new Date(mydata.mem[j][0]).getTime();
    }

    for(var j=0; j < mydata.throughput.length; j++){
        mydata.throughput[j][0] = new Date(mydata.throughput[j][0]).getTime();
        mydata.throughput[j][1] = mydata.throughput[j][1] * 1000;
    }

    if(neighbor in mydata.latency){
        for(var j=0; j < mydata.latency[neighbor].length; j++){
            mydata.latency[neighbor][j][0] = new Date(mydata.latency[neighbor][j][0]).getTime();
        }

        for(var j=0; j < mydata.jitter[neighbor].length; j++){
            mydata.jitter[neighbor][j][0] = new Date(mydata.jitter[neighbor][j][0]).getTime();
        }
    }

    for(var func in mydata.opticals){
        for(var j=0; j < mydata.opticals[func].length; j++){
            mydata.opticals[func][j][0] = new Date(mydata.opticals[func][j][0]).getTime();
        }
    }

    for(var rnti in mydata.ltes){
        for(var j=0; j < mydata.ltes[rnti].length; j++){
            mydata.ltes[rnti][j][0] = new Date(mydata.ltes[rnti][j][0]).getTime();
        }
    }

    return mydata;
}
function createOrUpdateSeries(chart, series_name, data){
    for(index in chart.series){
        if(chart.series[index].name == series_name)
            return chart.series[index].setData(data);
    }

    return chart.addSeries({
        name: series_name,
        data: data
    });
}
function getCol(chartid){
    var chart = $("#"+chartid);
    var bodybox = $(chart).parent();
    var box = $(bodybox).parent();
    var col = $(box).parent();

    return col
}
function showChart(chartid){
    var col = getCol(chartid);

    $(col).show();

}
function hideChart(chartid){
    var col = getCol(chartid);

    $(col).hide();
}
function collapseOptLte(){
    $("#optltecollapse").click();
}
function postDashboard(){
    var neighbor = $("input[name=neighbors]:checked").val();

    if(neighbor == null){
        $("input[name=neighbors]").first().attr("checked", true);
        neighbor = $("input[name=neighbors]:checked").val();
    }
    var timeInterval = $("#chart-data-interval").val();

    $.ajax({
        url: "/REST/network/pool/"+pool, 
        dataType: 'json',
        data: {
            timeInterval: timeInterval
        },
        success: function(result){
            mydata = result;
            // console.log(mydata);
            /** Formatting timestamp and units
             * CPU and Memory just needs to format timestamp
             * While throughput needs to adjust unit
             * */
            if(!mydata.error){
                mydata = fixDate4Chart(mydata, neighbor);

                var name_id = mydata.name.replace(' ', '').toLowerCase();
                (charts.cpu != null) ? charts.cpu.series[0].setData(mydata.cpu) : null;
                (charts.ram != null) ? charts.ram.series[0].setData(mydata.mem) : null;
                (charts.net != null) ? charts.net.series[0].setData(mydata.throughput) : null;
                if(neighbor in mydata.latency){
                    (charts.rtt != null) ? charts.rtt.series[0].setData(mydata.latency[neighbor]) : null;
                    (charts.jitter != null) ? charts.jitter.series[0].setData(mydata.jitter[neighbor]) : null;
                }
                else{
                    (charts.rtt != null) ? charts.rtt.series[0].setData([]) : null;
                    (charts.jitter != null) ? charts.jitter.series[0].setData([]) : null;
                }

                if(!(jQuery.isEmptyObject(mydata.opticals) && jQuery.isEmptyObject(mydata.ltes))){
                    if($("#optltebox").hasClass("collapsed-box"))
                        collapseOptLte();
                    
                    if(charts.optical != null)
                        if(!jQuery.isEmptyObject(mydata.opticals)){
                            showChart("chart-optical");
                            for(func in mydata.opticals){
                                createOrUpdateSeries(charts.optical, func, mydata.opticals[func]);
                            }
                        }
                        else
                            hideChart("chart-optical");
                    
                    if(charts.lte != null)
                        if(!jQuery.isEmptyObject(mydata.ltes)){
                            showChart("chart-lte");
                            for(rnti in mydata.ltes){
                                createOrUpdateSeries(charts.lte, rnti, mydata.ltes[rnti]);
                            }
                        }
                        else
                            hideChart("chart-lte");
                    }
                else{
                    if(!$("#optltebox").hasClass("collapsed-box"))
                        collapseOptLte();
                }

            }
            else{
                console.log(mydata)
            }

            dashboardTimer = setTimeout(postDashboard, pollTime);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}