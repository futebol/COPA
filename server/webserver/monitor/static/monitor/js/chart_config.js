options = {
    title: {
        text: ''
    },
    chart: {
        height: 160
    },
    exporting: {
        enabled: false,
    },

    credits: {
        enabled: false,
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            minute: '%H:%M:%S'
        },
        title: {
            text: 'Time'
        }
    },
    yAxis: {
        title: {
            text: 'Default'
        },
    },
    legend: {
        enabled: false,
    },

    time: {
        useUTC: false
    },
    rangeSelector: {
        buttons: [{
            count: 1,
            type: 'minute',
            text: '1M'
        },{
            count: 5,
            type: 'minute',
            text: '5M'
        },{
            type: 'all',
            text: 'All'
        },],
        inputEnabled: false,
        selected: 0
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 2010,
            marker: {
                enabled: false
            }
        },
    },

    series: [{
        name: '',
        color: 'red',
        data: []
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500,
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

};

cpu_options = jQuery.extend(true, {}, options);
ram_options = jQuery.extend(true, {}, options);
net_options = jQuery.extend(true, {}, options);
rtt_options = jQuery.extend(true, {}, options);
jit_options = jQuery.extend(true, {}, options);
lte_options = jQuery.extend(true, {}, options);
opt_options = jQuery.extend(true, {}, options);

cpu_options.yAxis.title.text = "Percentage";
cpu_options.yAxis.max = 100;
cpu_options.series[0].color = "blue";

ram_options.yAxis.title.text = "Percentage";
ram_options.yAxis.max = 100;
ram_options.series[0].color = "red";

net_options.yAxis.title.text = "Kbps";
net_options.series[0].color = "green";

rtt_options.yAxis.title.text = "milliseconds";
rtt_options.series[0].color = "red";

jit_options.yAxis.title.text = "milliseconds";
jit_options.series[0].color = "grey";

lte_options.yAxis.title.text = "dBi";

opt_options.yAxis.title.text = "bps";

