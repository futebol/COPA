function random_chart_data(first_fetch=true, ndata=10){
    var response = [];

    if(first_fetch){
        
        var todayseconds = new Date().getTime() / 1000;
        

        for(var i=0; i < ndata; i++){
            todayseconds++;
            //date.setSeconds(date.getSeconds() + 1);
            response.push([(new Date()).setTime(todayseconds*1000), Math.random()*100]);
        }
    }

    return response;
}

function chart_data(first_fetch=true, ndata=10){
    return random_chart_data(first_fetch, ndata);
}

var pollTime = 3000;
var poolsCharts = {};

$(document).ready(function(){
    var pools = $(".pool");
    

    for(var i = 0; i < pools.length; i++){
        var data_id = $(pools[i]).attr("data-id");

        cpu_options.series[0].data = chart_data();
        ram_options.series[0].data = chart_data();
        net_options.series[0].data = chart_data();

        poolsCharts[data_id] = {
            "cpu": Highcharts.chart(data_id+"-cpu", cpu_options),
            "ram": Highcharts.chart(data_id+"-ram", ram_options),
            "net": Highcharts.chart(data_id+"-net", net_options),
        };
    }

    postDashboard();
});

function postDashboard(){
    $.ajax({
        url: "/REST/network/dashboard", 
        dataType: 'json',
        success: function(result){
            mydata = result["data"];
            
            /** Formatting timestamp and units
             * CPU and Memory just needs to format timestamp
             * While throughput needs to adjust unit
             * */
            for(var i=0; i < mydata.length; i++){
                
                for(var j=0; j < mydata[i].cpu.length; j++){
                    mydata[i].cpu[j][0] = new Date(mydata[i].cpu[j][0]).getTime();
                }

                for(var j=0; j < mydata[i].mem.length; j++){
                    mydata[i].mem[j][0] = new Date(mydata[i].mem[j][0]).getTime();
                }

                for(var j=0; j < mydata[i].throughput.length; j++){
                    mydata[i].throughput[j][0] = new Date(mydata[i].throughput[j][0]).getTime();
                    mydata[i].throughput[j][1] = mydata[i].throughput[j][1] * 1000;
                }
            }

            for(var i=0; i < mydata.length; i++){
                var name_id = mydata[i].name.replace(' ', '').toLowerCase();
                poolsCharts[name_id].cpu.series[0].setData(mydata[i].cpu);
                poolsCharts[name_id].ram.series[0].setData(mydata[i].mem);
                poolsCharts[name_id].net.series[0].setData(mydata[i].throughput);
            }

            dashboardTimer = setTimeout(postDashboard, pollTime);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
}