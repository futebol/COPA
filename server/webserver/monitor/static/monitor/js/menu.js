$("#alert-test").click(function(){
    title = "Alert!";
    text = "Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart."
    type = "error";
    setAlert(type, title, text);
});

function setAlert(type, title, text){
    hash = Math.random().toString(36).substring(2,15) + Math.random().toString(36).substring(2,15);

    if(type == "error")
        className = "danger";
    else if(type == "success"){
        className = "success";
    }

    alert = $("<div>").addClass("alert alert-"+className+" alert-dismissible")
        .attr("alert-hash", hash);
    button = $("<button>")
        .attr("type","button")
        .attr("data-dismiss", "alert")
        .attr("aria-hidden", "true")
        .addClass("close")
        .html("×");
    errorImage = $("<i>")
        .addClass("icon fa fa-ban");

    titleBox = $("<h4>")
        .append(errorImage)
        .append(title);
    
    $(alert).append(button)
        .append(titleBox)
        .append(text);

    console.log("Alert!");
    $(".notification-box").append(alert);

    setTimeout(function(){
        alertDismiss(hash);
    }, 3000);
}

function alertDismiss(hash){
    console.log("Dismiss called");
    $("div[alert-hash="+hash+"]").fadeOut(2000);
}