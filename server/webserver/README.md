# COPA - Container Orchestration and Provisioning Architecture
IMPORTANT: THIS TUTORIAL WAS TESTED WITH PYTHON3 ON UBUNTU 16.04 LTS, 17.04 and 17.10!

First, you must clone this repository into `$COPA_HOME`:

```bash
sudo git clone https://gitlab.com/futebol/COPA.git $COPA_HOME
```

## Install Requirements

```bash
sudo apt-add-repository ppa:ubuntu-lxc/stable #This adds the latest stable version of LXD and its dependencies.
sudo apt update
sudo apt install -y python3-pip nodejs nodejs-legacy npm lxd criu
sudo pip3 install -r requirements.txt
cd $COPA_HOME
sudo npm install express socket.io pty term.js
```

To setup the container pools, refer to the README.md file on the `live-migration` folder.

Make your user part of the lxd group, so you dont need SUDO privileges to work with LXD:

```bash
sudo usermod -aG lxd $USER
```

## Initial LXD config
Run the following command and follow its instructions. When it ask if you want to LXD to be available over the network, answer yes and setup the standard access IP, PORT and PASSWORD.

```bash
lxd init
```

After that, you must use the following commands so LXD can be used in every host:

```bash
lxc config set core.https_address [::]:8443
lxc config set core.https_allowed_origin [::]:8443
lxc config set core.https_allowed_methods "GET, POST, PUT, DELETE, OPTIONS"
lxc config set core.https_allowed_headers "Origin, X-Requested-With, Content-Type, Accept"
lxc config set core.https_allowed_credentials "true"
```

This must be done in all LXD Hosts (Image and Container pools)!

## Fix certificates
### COPA Certificate Authority - `COPA-CA.crt` and COPA Server Certificate - `lxd.crt`
This is done at the COPA Server host.

**Change the IP located at the `openssl.cnf` file (`subjectAltName = IP:192.168.200.156`),
so it matches the COPA Server IP.** After that, run:

```bash
sudo sh makeCOPA-CA.sh
```

This wil create the appropriate Certificate Authority to be used with COPA.

After that, run:

```bash
sudo sh makeCOPAcerts.sh
```

Now, copy this folder to the different Image and Container pools
(`scp -r $COPA_HOME/certs <SERVER>@<HOST>:~/`). You will need it.

### Container pools Certificate - `LXD-SERVER.crt`
This is done at the different Image and Containers pools.

**Change the IP located at the `openssl.cnf` file (`subjectAltName = IP:192.168.200.156`),
so it matches the Image/Container host that you are setting up.** After that, run:

```bash
sudo sh makeCert-pool.sh
```

Also, you need to add the COPA Server certificate (`lxd.crt`) to the LXD trusted
hosts list on each LXD host, using:

```bash
lxc config trust add certs/lxd.crt
```

Now your COPA Server and Image/Container pools are ready to use.

## Adding Servers

Change the `servers.txt` file located in  `$COPA_HOME/`, naming the servers by https connection `<SERVERNAME>;<SERVERIP>:<PORT>`. For example:

```
Server1;192.168.1.50:8443
```

## Build the database
Execute the following commands:

```
python3 $COPA_HOME/copa/manage.py makemigrations
python3 $COPA_HOME/copa/manage.py migrate
```

## Starting COPA
Modify the following files:

* `$COPA_HOME/container_site/core/COPA_general.py`:
```
COPA_HOME = ""  # Location of the COPA home.
COPA_HOST = '192.168.200.223'  # COPA's host IP address.
```
* `server_socket.js`:
```
var COPA_HOME = ""  //Location of the COPA home.
```

To start COPA, execute:

```
python3 $COPA_HOME/copa/manage.py runserver 0.0.0.0:8000
python3 $COPA_HOME/copa/manage.py shell < core/refresh_service.py
node $COPA_HOME/copa/core/static/core/xterm/dist/server_socket.js #FIX HERE. NEED TO `cd $COPA_HOME/copa/core/static/core/xterm/dist/` and then `node server_socket.js`
```

Access via webbrowser `HOST_IP:8000/core/` or `HOSTNAME:8000/core/`

## Creating images to use with COPA

```
lxc image copy <SOURCE> local: --alias <IMG_ALIAS> --public
```
