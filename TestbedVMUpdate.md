# How to update the VM in the testbed

This tutorial is to guide through the Testbed VM update.

## Remove old COPA
In order to update the COPA VM you need to remove the old code.

The code is in */copa* in the COPA Server and in the */pool* in the COPA Pool.

In COPA Server do the following:

```
cd /copa
rm -r *
```


In COPA Pool do the following:

```
rm -r /pool
sudo mkdir /copa
sudo chown -R copa:copa /copa
```


As you can see, in the COPA Pool, we changed the folder name from pool to copa.

## Download new COPA code
The following steps are the same for both Server and Pool.
Enter in the COPA folder and download the code from github. Do not forget the **dot** in the final of the git clone command.

```
cd /copa
git clone https://gitlab.com/futebol/COPA.git .
```


## Deploy Pools
It is important to deploy the pools before deploying the server and, when turning them off, you need to turn off first the Server and then the pools.

```
cd /copa/pool
./pool.py start
```


You can check the logs at the */copa/pool/log* folder.

## Deploy Server
We need to prepare the django database so we can start our webserver later.

```
cd /copa/server/webserver
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py loaddata initconfig
```


Add a user in the database. In our example, we created a user named *copa* with a password *copa*.

`echo "from django.contrib.auth.models import User; User.objects.create_superuser('copa', 'copa@example.com', 'copa')" | python3 manage.py shell`


Change the config file to your settings

```
cd /copa/server
vim config.json
```
``

The file has the following structure.
*{ "user": "copa", "password": "copa", "server_ip": "10.0.2.11", "pools": [ { "label": "Server1", "ip": "10.0.2.9" }, { "label": "Server2", "ip": "10.0.2.10" } ] }*
In user and password you need to put the standard user and password of yours machines hosting the pools. In server_ip you must put the ip from the COPA Server. In the pools list you need to add objects with label and ip of the existing COPA Pools.

**It is important that the labels do not have spaces or special characters.**

After setting up the IPs and users of the COPA Servers and Pools, its time to run some commands in other to execute the configuration that you setup in the configuration file. Execute the following command in /copa/server

`./copa.py configure`

In order to make COPA enabled for acessing LXD API from the COPA Pools. We need to setup some certificates. Fortunatelly, we have a automated script that create the certificate and distribute it automatically to the COPA Pools through ssh. In the folder /copa/server/certs you need to run:

`./automated_cert.py`

Then, you can comeback in the /copa/server and run:

**It is important that the pools are already running before you start the server.**

`./copa.py start`